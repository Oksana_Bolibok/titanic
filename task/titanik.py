import pandas as pd


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


def get_filled():
    df = get_titatic_dataframe()
    median_age_by_title = {
        'Mr.': 0,
        'Mrs.': 0,
        'Miss.': 0
    }

    for title in median_age_by_title.keys():
        median_age_by_title[title] = df[df['Name'].str.contains(title)]['Age'].median()

    for title in median_age_by_title.keys():
        df.loc[df['Name'].str.contains(title) & df['Age'].isnull(), 'Age'] = median_age_by_title[title]

    result = [(title, len(df[df['Name'].str.contains(title) & df['Age'].isnull()]), round(median_age_by_title[title])) for title in median_age_by_title.keys()]

    return result

